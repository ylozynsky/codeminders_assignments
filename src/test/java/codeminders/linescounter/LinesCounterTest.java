/*
 * This is a test
 */
package codeminders.linescounter;

import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import org.junit.Test;

public class LinesCounterTest {
	@Test
	public void shouldCountLinesOfDir() throws IOException {
		final String dir = "src/test/resources/root/";
		final String expectedOutput = "root : 8\n"
				+ " subfolder2 : 5\n"
				+ "  Hello.java : 5\n"
				+ " subfolder1 : 3\n"
				+ "  Dave.java : 3\n\n";
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		PrintStream out = new PrintStream(baos);
		new LinesCounter().countLines(out, dir);
		final String actualOutput = baos.toString();
		assertTrue(expectedOutput.equals(actualOutput));
	}

	@Test
	public void shouldCountLinesOfSingleFile() throws IOException {
		final String dir = "src/test/resources/root/subfolder1/Dave.java";
		final String expectedOutput = "Dave.java : 3\n\n";
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		PrintStream out = new PrintStream(baos);
		new LinesCounter().countLines(out, dir);
		final String actualOutput = baos.toString();
		assertTrue(expectedOutput.equals(actualOutput));
	}
}
