package codeminders.linescounter;

public class SingleLineCounter {
	boolean lineCounted;
	private FileDirEntry entry;
	public SingleLineCounter(FileDirEntry entry) {
		this.entry = entry;
	}

	public void countLine() {
		if (!lineCounted) {
			lineCounted = true;
			entry.incrementLinesCount(1);
		}
	}
	public void setLineCounted(boolean lineCounted) {
		this.lineCounted = lineCounted;
	}
}
