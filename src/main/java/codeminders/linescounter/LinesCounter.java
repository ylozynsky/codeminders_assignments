package codeminders.linescounter;

import static java.lang.Character.isWhitespace;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Path;
import java.nio.file.Paths;

public class LinesCounter {
	public FileDirEntry countLines(final String fileDirName) throws IOException {
		final Path path = Paths.get(fileDirName);
		final File file = path.toFile();
		if (file.isDirectory()) {
			return countDirLines(file);
		} else {
			return countFileLines(file);
		}
	}

	private String getExtension(String fileName) {
		int i = fileName.lastIndexOf('.');
		return i > 0 ? fileName.substring(i + 1) : null;
	}

	public FileDirEntry countDirLines(final File dir) throws IOException {
		FileDirEntry entry = new FileDirEntry(dir.getName());
		for (File file : dir.listFiles()) {
			if (file.isDirectory()) {
				entry.addEntry(countDirLines(file));
			} else if ("java".equals(getExtension(file.getName()))){
				entry.addEntry(countFileLines(file));
			} else {
				// log.info("skipping non java-source file");
			}
		}
		return entry;
	}

	enum CountingState {
		COUNTING,
		STRING_LITERAL,
		MULTILINE_COMMENT
	}

	public FileDirEntry countFileLines(final File file) throws IOException {
		FileDirEntry result = new FileDirEntry(file.getName());
		BufferedReader reader = new BufferedReader(new FileReader(file));
		String line = reader.readLine();
		SingleLineCounter singleLineCounter = new SingleLineCounter(result);
		CountingState countingState = CountingState.COUNTING;
		while (line != null) {
			lineCountingLoop:
			for (int charIndex = 0; charIndex < line.length(); charIndex ++) {
				char ch = line.charAt(charIndex);
				char nextChar = charIndex < line.length() - 1 ?
						line.charAt(charIndex + 1) : 0;
				char prevChar = charIndex > 0 ? line.charAt(charIndex - 1) : 0;
				switch (ch) {
				case '/':
					if (countingState != CountingState.MULTILINE_COMMENT) {
						if (countingState != CountingState.STRING_LITERAL) {
							if (nextChar == '/') {
								// single-line comment, break the line scanning
								break lineCountingLoop;
							}
						}
					} else { // it was multiline comment, maybe comment has ended
						if (prevChar == '*') {
							countingState = CountingState.COUNTING;
						}
					}
					break;
				case '*':
					if (countingState != CountingState.STRING_LITERAL) {
						if (charIndex > 0 && line.charAt(charIndex - 1) == '/') {
							countingState = CountingState.MULTILINE_COMMENT;
						} else if (countingState != CountingState.MULTILINE_COMMENT){
							singleLineCounter.countLine();
						}
					} else {
						singleLineCounter.countLine();
					}
					break;
				case '"':
					if (countingState != CountingState.MULTILINE_COMMENT) {
						if (countingState == CountingState.STRING_LITERAL) {
							countingState = CountingState.COUNTING;
						} else {
							countingState = CountingState.STRING_LITERAL;
						}
						singleLineCounter.countLine();
					}
					break;
				default:
					if (countingState != CountingState.MULTILINE_COMMENT) {
						if (!isWhitespace(ch)) {
							singleLineCounter.countLine();
						}
					}
					break;
				}
			}
			line = reader.readLine();
			singleLineCounter.setLineCounted(false);
		}
		reader.close();
		return result;
	}

	public static void main(String[] args) throws IOException {
		if (args.length == 1) {
			String path = args[0]; // "src/test/resources/root/subfolder2/Hello.java"; //
			new LinesCounter().countLines(System.out, path);
			
		} else {
			System.out.println("Pass single argument: java source file name or java source directory");
			return;
		}
	}

	// for test cases allow to pass any PrintStream and path
	void countLines(PrintStream out, String path) throws IOException {
		out.println(new LinesCounter().countLines(path));
	}
}
