package codeminders.linescounter;

import java.util.ArrayList;
import java.util.List;

public class FileDirEntry {
	private final String name;
	private int linesCount;
	private List<FileDirEntry> subEntries;

	public FileDirEntry(String name) {
		this.name = name;
	}
	
	public void addEntry(FileDirEntry subentry) {
		if (subEntries == null) {
			subEntries = new ArrayList<>();
		}
		subEntries.add(subentry);
		incrementLinesCount(subentry.getLinesCount());
	}
	
	public List<FileDirEntry> getSubEntries() {
		return subEntries;
	}

	public int getLinesCount() {
		return linesCount;
	}

	public void incrementLinesCount(int linesCount) {
		this.linesCount += linesCount;
	}
	
	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		toString(result, 0);
		return result.toString();
	}
	
	private void toString(StringBuilder result, int offset) {
		for(int i = 0; i < offset; i++) {
			result.append(' ');
		}
		result.append(name).append(" : ").append(linesCount).append("\n");
		if (subEntries != null) {
			for(FileDirEntry subEntry : subEntries) {
				subEntry.toString(result, offset + 1);
			}
		}
	}
}
